/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import db.MapperException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author niels
 */
public class HearthstoneServiceTest {
    
    private Deck deck;
    private Card card;
    private HearthstoneService service;
    
    public HearthstoneServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        service = new HearthstoneService("jpa");
    }
    
    @After
    public void tearDown() {
        try {
            service.removeCard(card.getId());
            service.removeDeck(deck.getId());
        } catch (MapperException ex) {
            Logger.getLogger(HearthstoneServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    @Test
    public void testStoreDeck() throws Exception {
       
       
        deck = new Deck();
        deck.setName("Naam");
        deck.setPlayerClass("playerClass");
        long deckId = service.addDeck(deck);
        Deck persistedDeck = service.getDeck(deckId);
        
        card = new Card("Ragnaros", "Minion", "Can't Attack. At the end of turn deal 8 damage to a random enemy character", "All classes", 8, 8, 8);
        
        long cardId = service.addCard(card);
        System.out.println(cardId);
        persistedDeck.addCard(cardId);
        persistedDeck.addCard(cardId);
        
        service.updateDeck(persistedDeck);
     
        Deck persistedDeckAfterUpdate = service.getDeck(deckId);
        
        assertTrue(persistedDeckAfterUpdate.getSize() == 2);
    
       
    }
    
   
    
}
