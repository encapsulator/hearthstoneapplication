/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import domain.Card;
import domain.Deck;

/**
 *
 * @author niels
 */
public abstract class MapperLocator {

    private static MapperLocator instance;
    protected Mapper<Card> cardMapper;
    protected Mapper<Deck> deckMapper;

    public static void loadLocator(MapperLocator locator) {
        instance = locator;
    }

    public static Mapper<Card> cardMapper() {
        return instance.cardMapper;
    }
    
    public static Mapper<Deck> deckMapper() {
        return instance.deckMapper;
    }
   
}
