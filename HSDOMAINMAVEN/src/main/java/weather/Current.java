/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author niels
 */
@XmlRootElement
public class Current {
    private City city;
    private Temperature temperature;
    
    public Current() {}

    public Current(City city, Temperature temperature) {
        this.city = city;
        this.temperature = temperature;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Temperature getTemperature() {
        return temperature;
    }
    
    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }
    
    
    
}
