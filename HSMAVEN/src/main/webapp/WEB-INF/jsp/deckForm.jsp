
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <title>
            Hearthstone | 
            <spring:message code="title.addDeck">
            </spring:message>
        </title>
        <jsp:include page="title.jsp" />
        <script src="${pageContext.request.contextPath}/js/deckForm.js" language="javascript" type="text/javascript"></script>

    </head>

    <body>

        <div id="wrapper">
            <jsp:include page="menu.jsp" />
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">

                            <form:form id="deckForm" action="${pageContext.request.contextPath}/deckForm.htm" method="post" modelAttribute="deck">
                                <form:errors path="*" cssClass="alert alert-danger" element="div" />
                                <c:choose>
                                    <c:when test="${deck.id > 0}">
                                        <h3><spring:message code="deck.update">
                                            </spring:message></h3>
                                            <form:hidden path="id" value="${deck.id}"/>
                                        <input type="hidden" name="dispatcher" value="updateDeck"/>
                                    </c:when>
                                    <c:otherwise>
                                        <h3><spring:message code="deck.add">
                                            </spring:message></h3>
                                        <input type="hidden" name="dispatcher" value="createDeck"/>
                                    </c:otherwise>
                                </c:choose>

                                <div class="form-group">
                                    <form:label path="name"><spring:message code="deck.name">
                                        </spring:message>: </form:label>
                                    <form:input path="name" class="form-control"></form:input>
                                    </div>


                                    <div class="form-group">
                                    <form:label  path="playerClass"><spring:message code="deck.playerClass">
                                        </spring:message>: </form:label>
                                    <form:select id="deckClass" class="form-control" path="playerClass">
                                        <form:option value="Priest">Priest</form:option>
                                        <form:option value="Shaman">Shaman</form:option>
                                        <form:option value="Mage">Mage</form:option>
                                        <form:option value="Warrior">Warrior</form:option>
                                        <form:option value="Warlock">Warlock</form:option>
                                        <form:option value="Paladin">Paladin</form:option>
                                        <form:option value="Rogue">Rogue</form:option>
                                        <form:option value="Druid">Druid</form:option>
                                        <form:option value="Hunter">Hunter</form:option>
                                    </form:select>
                                </div>

                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><spring:message code="card.name">
                                                </spring:message></th>
                                            <th><spring:message code="card.playerClass">
                                                </spring:message></th>
                                            <th><spring:message code="card.remove">
                                                </spring:message></th>
                                            <th>
                                                <spring:message code="card.amount">
                                                </spring:message>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableData">
                                         <c:if test="${not empty existingCards}">
                                         
                                            <c:forEach var="card" items="${existingCards}">
                                                <tr class="newRow" data-id="${card.id}" data-cardname="${card.name}">
                                                    <td class="cardName">${card.name}</td>
                                                    <td>${card.playerClass}</td>
                                                    <td><a class='removeCard' href='#'>Remove</a></td>
                                                    <td class="cardAmount">${card.count}</td>
                                                </tr>
                                            </c:forEach>
                                            <script>populateCards();</script>
                                        </c:if>
                                    </tbody>

                                </table>    

                                <div class="form-group">
                                    <label for="cardSelect"> <spring:message code="deck.selectCard">
                                        </spring:message>:</label>
                                    <select class="form-control" id="cardSelect" size="10">
                                        <option value="selectCard" >Select a card</option>   
                                    <c:if test="${not empty cards}">
                                        <c:forEach var="card" items="${cards}">
                                            <option data-id="${card.id}" data-playerclass="${card.playerClass}" value="${card.name}">${card.name}</option>
                                        </c:forEach>
                                    </c:if>
                                    </select>
                                </div>


                            </form:form>
                            <div class="form-group">
                                <button id="sendForm" class="btn btn-primary"><spring:message code="form.send">
                                    </spring:message></button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

    </body>

</html>
