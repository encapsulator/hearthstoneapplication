
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <title>
            Hearthstone | 
            <spring:message code="title.deck"> detail
            </spring:message>
        </title>
        <jsp:include page="title.jsp" />
        <jsp:include page="dataTable.jsp"/>

    </head>

    <body>

        <div id="wrapper">
            <jsp:include page="menu.jsp" />
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="#menu-toggle" class="btn btn-default pull-right" id="menu-toggle">
                                <spring:message code="menu.toggle">
                                </spring:message>
                            </a>
                            <form  method="POST" action="${pageContext.request.contextPath}/lang/">
                                <label for="language">
                                    <spring:message code="language">
                                    </spring:message>
                                </label>
                                <select id="language" name="lang" onchange="submit()">
                                    <option value="en_GB" ${pageContext.response.locale == 'en_GB' ? 'selected' : ''}>English</option>
                                    <option value="nl_BE" ${pageContext.response.locale == 'nl_BE' ? 'selected' : ''}>Nederlands</option>
                                </select>
                            </form>

                            <h1>
                                <spring:message code="title.deck">
                                </spring:message> detail
                            </h1>
                                
                                <p>
                                    <strong><spring:message code="deck.name"></spring:message>: </strong> ${name} <br/>
                                    <strong><spring:message code="deck.playerClass"></spring:message>: </strong> ${playerClass}
                                </p>
                                
                                <h3><spring:message code="title.cards"></spring:message></h3>    
                                
                            <table class="table table-hover" id="table">
                                <thead>
                                    <tr>
                                        <th>
                                            <spring:message code="card.name">
                                            </spring:message>
                                        </th>
                                        <th>
                                        <spring:message code="deckDetail.amount">
                                            </spring:message>
                                        </th>
                                        
                                        
                                    </tr>
                                </thead>
                        
                                <tbody id="tableid">
                                    <c:if test="${not empty cards}">
                                        
                                        <c:forEach var="card" items="${cards}">
                                            <tr>
                                                
                                                <td>${card.name}</td>
                                                <td>${card.count}</td>
                                                
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>

            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
                                    $("#menu-toggle").click(function (e) {
                                        e.preventDefault();
                                        $("#wrapper").toggleClass("toggled");
                                    });
        </script>

    </body>

</html>