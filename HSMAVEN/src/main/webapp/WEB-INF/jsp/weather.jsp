<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<p><spring:message code="itis">
</spring:message> ${current.temperature.value} <spring:message code="degreesoutsidein"></spring:message> ${current.city.name}. 
    <c:choose>
        <c:when test="${current.temperature.value > 20}">
        <spring:message code="playoutside">
        </spring:message>
    </c:when>
    <c:otherwise>
        <spring:message code="playinside">
        </spring:message>
    </c:otherwise>
</c:choose></p>